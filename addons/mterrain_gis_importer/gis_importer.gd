class_name GISImporter

const NON_MAP_AREA_HEIGHT : float = -10000

var meters_per_pixel : float = 463.246
var source_min_height : float = -5052
var source_max_height : float = 4375
var target_region_size : int = 1024
var target_region_px : int = target_region_size * 2
var region_size_meters : float = target_region_size * 32
var accuracy : float = 0.01
var mterrain_data_path : String
var scale : float = 1
var interpolation_mode : Image.Interpolation = Image.INTERPOLATE_CUBIC
var plugins : Array[GISImporterPlugin] = []

func _init(mterrain_data : String = "res://mterrain/data/"):
	mterrain_data_path = mterrain_data

func add_plugin(plugin : GISImporterPlugin) -> void:
	plugins.push_back(plugin)

func import(file_path : String) -> void:
	var img : Image = Image.load_from_file(file_path)
	if img == null:
		printerr("Failed to load file")
		return
	print("importing " + file_path)
	img = _convert_to_rf(img)
	_build(img)
	write_config_file()
	print("import finished!")

func write_config_file() -> void:
	var file_path = mterrain_data_path.path_join(".save_config.ini")
	var config : ConfigFile = ConfigFile.new()
	if FileAccess.file_exists(file_path):
		if config.load(file_path) != OK:
			printerr("Can not load conf file")
	config.set_value("heightmap", "accuracy", accuracy)
	config.set_value("heightmap", "file_compress", false)
	config.set_value("heightmap", "compress_qtq", true)
	config.save(file_path)
	print(file_path + " was created")

func _convert_to_rf(img : Image) -> Image:
	print("converting heightmap to rf...")
	var img_size = img.get_size()
	img.convert(Image.FORMAT_RF)
	var data = img.get_data().to_float32_array()
	_convert_to_meter(data)
	return Image.create_from_data(img_size.x, img_size.y, false, Image.FORMAT_RF, data.to_byte_array())

func _convert_to_meter(data : PackedFloat32Array) -> void:
	var min : float = 1
	var max : float = 0
	for i in range(0, data.size()):
		if data[i] > max:
			max = data[i]
		if data[i] < min:
			min = data[i]
	var total_spectrum : float = (source_max_height - source_min_height) / (max - min)
	var zero_value : float = source_min_height - min * total_spectrum
	for i in range(0, data.size()):
		data[i] = (zero_value + data[i] * total_spectrum) * scale

func _build(img : Image) -> void:
	var region_size_px : float = region_size_meters / (meters_per_pixel * scale)
	var regions : Vector2i = ceil((img.get_size() - Vector2i(1, 1)) / region_size_px)
	print("determined regions: " + str(regions.x) + "x" + str(regions.y) + " using " + str(region_size_px) + " source px for each region that is " + str(region_size_meters) + "m x " + str(region_size_meters) + "m")
	var threads : Array[Thread] = []
	for i in range(0, OS.get_processor_count()):
		threads.push_back(Thread.new())
	for y in range(0, regions.y):
		var progression : String = "%.1f" % (y * 100.0 / regions.y)
		print(progression + "% finished")
		if y == 2:
			print("expected total map data of " + str(_estimate_memory(regions)) + "MB")
		for x in range(0, regions.x):
			#if ResourceLoader.exists(mterrain_data_path.path_join(_build_region_file_name(x, y))):
				#continue
			var region_img : Image = _build_region_image(img, x, y, round(region_size_px))
			var next_thread : Thread = null
			while next_thread == null:
				for thread in threads:
					if !thread.is_alive():
						next_thread = thread
						break
			next_thread.wait_to_finish()
			next_thread.start(_build_region.bind(region_img, img, x, y, round(region_size_px)))
	for thread in threads:
		thread.wait_to_finish()

func _build_region(region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> void:
	var mres : MResource = MResource.new()
	for plugin in plugins:
		region_img = plugin.run(self, region_img, img, x, y, region_size_px)
	mres.insert_heightmap_rf(region_img.get_data(), accuracy, true, MResource.FILE_COMPRESSION_NONE)
	ResourceSaver.save(mres, mterrain_data_path.path_join(_build_region_file_name(x, y)))

func _build_region_file_name(x : int, y : int) -> String:
	return "x" + str(x) + "_y" + str(y) + ".res"

func _build_region_image(img : Image, x : int, y : int, region_size_px : int) -> Image:
	var start_pos : Vector2i = _build_start_pos_img(x, y, region_size_px)
	var image_size : Vector2i = Vector2i(region_size_px, region_size_px)
	var region_img : Image = img.get_region(Rect2i(start_pos, image_size))
	var valid_region : Vector2i = img.get_size() - start_pos
	_lower_end_of_source_image(region_img, valid_region, image_size)
	region_img.resize(target_region_px, target_region_px, interpolation_mode)
	return region_img

func _build_start_pos_img(x : int, y : int, region_size_px : int) -> Vector2i:
	return Vector2i(max(x * region_size_px - 1, 0), max(y * region_size_px - 1, 0))

func _lower_end_of_source_image(region_img : Image, valid_region : Vector2i, image_size : Vector2i) -> void:
	if valid_region.x < image_size.x:
		region_img.fill_rect(Rect2i(Vector2i(valid_region.x, 0), image_size), Color(NON_MAP_AREA_HEIGHT, 0, 0))
		for i in range(0, (source_min_height - NON_MAP_AREA_HEIGHT) / 1000):
			region_img.fill_rect(Rect2i(Vector2i(valid_region.x + i, 0), Vector2i(1, image_size.y)), Color(source_min_height - 1000 * i, 0, 0))
	if valid_region.y < image_size.y:
		region_img.fill_rect(Rect2i(Vector2i(0, valid_region.y), image_size), Color(NON_MAP_AREA_HEIGHT, 0, 0))
		for i in range(0, (source_min_height - NON_MAP_AREA_HEIGHT) / 1000):
			region_img.fill_rect(Rect2i(Vector2i(0, valid_region.y + i), Vector2i(valid_region.x, 1)), Color(source_min_height - 1000 * i, 0, 0))

func _estimate_memory(regions : Vector2i) -> int:
	var total_size : float = 0
	for x in range(0, regions.x):
		var region_file : String = _build_region_file_name(x, 0)
		var file_path : String = mterrain_data_path.path_join(region_file)
		var file : FileAccess = FileAccess.open(file_path, FileAccess.READ)
		total_size += file.get_length()
	return total_size * regions.y / (1024 * 1024)

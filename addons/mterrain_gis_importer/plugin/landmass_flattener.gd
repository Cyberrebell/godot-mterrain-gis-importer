extends GISImporterPlugin

class_name LandmassFlattener

var coast_distance : float
var target_height : float
var region_segments : int = 32

func _init(min_coast_distance : float = 4000, land_target_height : float = 10):
	coast_distance = min_coast_distance
	target_height = land_target_height

func run(importer : GISImporter, region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> Image:
	var start_pos_img : Vector2i = importer._build_start_pos_img(x, y, region_size_px)
	var color : Color = Color.BLACK
	color.r = target_height
	var region_img_size : Vector2i = region_img.get_size()
	var step_size : int = region_img_size.x / region_segments
	var step_size_img : int = step_size * float(region_size_px) / region_img_size.x
	var coast_distance_img_px : int = ceil(coast_distance / importer.meters_per_pixel)
	for yi in range(0, region_img_size.y, step_size):
		for xi in range(0, region_img_size.x, step_size):
			var rect : Rect2i = Rect2i(xi, yi, step_size, step_size)
			if !has_water(region_img, rect):
				var rect_img_pos : Vector2i = Vector2i(start_pos_img.x + region_size_px * (float(xi) / region_img_size.x), start_pos_img.y + region_size_px * (float(yi) / region_img_size.y))
				var coast_check_rect : Rect2i = Rect2i(rect_img_pos.x - coast_distance_img_px, rect_img_pos.y - coast_distance_img_px, step_size_img + 2 * coast_distance_img_px, step_size_img + 2 * coast_distance_img_px)
				if coast_check_rect.position.x < 0:
					coast_check_rect.position.x = 0
					coast_check_rect.size.x -= coast_distance_img_px
				if coast_check_rect.position.y < 0:
					coast_check_rect.position.y = 0
					coast_check_rect.size.y -= coast_distance_img_px
				if !has_water(img, coast_check_rect):
					region_img.fill_rect(rect, color)
	return region_img

func has_water(img : Image, rect : Rect2i) -> bool:
	var section : Image = img.get_region(rect)
	var data = section.get_data().to_float32_array()
	for i in range(0, data.size()):
		if data[i] < 0:
			return true
	return false

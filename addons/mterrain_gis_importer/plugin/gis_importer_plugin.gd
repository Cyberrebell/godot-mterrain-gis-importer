class_name GISImporterPlugin

func run(importer : GISImporter, region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> Image:
	printerr("used GISImporterPlugin plugin is missing a custom run() function")
	return region_img

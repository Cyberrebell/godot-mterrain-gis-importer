# Godot MTerrain GIS importer
Tool to import GIS GeoTIFF into [MTerrain](https://github.com/mohsenph69/Godot-MTerrain-plugin) data format.

The terrain will match 1:1 real world scale by default.
![](https://gitlab.com/Cyberrebell/godot-mterrain-gis-importer/-/wikis/uploads/2906295990ce007ff2e1863b5184cdbf/norway.jpg)

## How to use
1. Download your GeoTIFF Grid from https://download.gebco.net/
2. Convert the .tif to .exr using GIMP
3. Run the importer (and adjust settings and add plugins as you need):

```gdscript
func _ready():
    var importer = GISImporter.new()
    importer.source_min_height = -1058
    importer.source_max_height = 1642
    importer.accuracy = 4
    importer.add_plugin(HeightLimiter.new(-100))
    importer.add_plugin(LandmassFlattener.new())
    importer.import("res://norway.exr")
```
4. Set your MTerrain node **region_size** 1024 and **min_h_scale** 16

## Get relevant GeoTIFF metadata
```shell
gdalinfo <file.tif> | grep 'Min='
```
Will print something like this:
```shell
Min=-5052.000 Max=4375.000
```
Or open the .tif in QGIS

## Plugins

### HeightLimiter
* This plugin can set a upper or lower limit to your terrain
* Useful to save memory if you don't need underwater geometry

### LandmassFlattener
* This plugin does cut sections from your heightmap that exceed a specific distance from the sea (<0m height)
* Useful to save memory in naval games where you only see the coast terrain

## You own plugin
* Create your own class that `extends GISImporterPlugin`
* Create a **run()** method that does match the parent signature
* Check the existing plugins to understand how to operate
* Your plugin will run in multiple CPU threads. If you use non threadsave resources consider using a Mutex

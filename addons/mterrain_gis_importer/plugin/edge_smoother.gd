extends GISImporterPlugin

class_name EdgeSmoother

var border_smoothing_pixels : float = 3

func run(importer : GISImporter, region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> Image:
	return _smooth_region_edges(importer, region_img, img, x, y, region_size_px)

func _smooth_region_edges(importer : GISImporter, region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> Image:
	var compare_img : Image = importer._build_region_image(img, x, y + 1, region_size_px)
	var y_max : int = region_img.get_size().y - 1
	for xi in range(0, region_img.get_size().x):
		var compare_value : float = compare_img.get_pixel(xi, 0).r
		_smoothen_color_y(region_img, xi, range(y_max, y_max - border_smoothing_pixels, -1), compare_value)
	compare_img = importer._build_region_image(img, x, y - 1, region_size_px)
	for xi in range(0, region_img.get_size().x):
		var compare_value : float = compare_img.get_pixel(xi, compare_img.get_size().y - 1).r
		_smoothen_color_y(region_img, xi, range(0, border_smoothing_pixels), compare_value)
	return compare_img

func _smoothen_color_y(img : Image, x : int, y_range : Array, compare_value : float) -> void:
	var y_inside : int = y_range[y_range.size() - 1]
	var max_value : float = img.get_pixel(x, y_inside).r
	var new_value : float = (max_value + compare_value) / 2
	var step_size : float = abs((max_value - new_value) / (y_inside - y_range.front()))
	for yi in y_range:
		var color : Color = img.get_pixel(x, yi)
		color.r = new_value
		img.set_pixel(x, yi, color)
		new_value = move_toward(new_value, max_value, step_size)

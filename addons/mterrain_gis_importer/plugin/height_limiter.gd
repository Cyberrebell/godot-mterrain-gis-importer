extends GISImporterPlugin

class_name HeightLimiter

var min_display_height : float
var max_display_height : float

func _init(min_height : float = -500, max_height : float = 10000):
	min_display_height = min_height
	max_display_height = max_height

func run(importer : GISImporter, region_img : Image, img : Image, x : int, y : int, region_size_px : int) -> Image:
	var region_img_size : Vector2i = region_img.get_size()
	var data = region_img.get_data().to_float32_array()
	for i in range(0, data.size()):
		if data[i] < min_display_height:
			data[i] = min_display_height
		elif data[i] > max_display_height:
			data[i] = max_display_height
	return Image.create_from_data(region_img_size.x, region_img_size.y, false, Image.FORMAT_RF, data.to_byte_array())

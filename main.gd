extends Node3D

func _ready():
	var importer = GISImporter.new()
	importer.source_min_height = -1225
	importer.source_max_height = 1642
	importer.accuracy = 1
	#importer.interpolation_mode = Image.INTERPOLATE_BILINEAR
	#importer.add_plugin(HeightLimiter.new(-100))
	#importer.add_plugin(LandmassFlattener.new())
	importer.import("res://norway.exr")
